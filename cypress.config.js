const { defineConfig } = require("cypress");

module.exports = defineConfig({
	projectId: "pcbke2e",
	adminPassword: "admin",
	testUser: "Administrator",
	defaultCommandTimeout: 20000,
	pageLoadTimeout: 15000,
	video: true,
	videoUploadOnPasses: false,
	e2e: {
		// We've imported your old cypress plugins here.
		// You may want to clean this up later by importing these.
		setupNodeEvents(on, config) {
			return require("./cypress/plugins/index.js")(on, config);
		},
		specPattern: ["./cypress/integration/*.js", "**/ui_test_*.js"],
	},
});
