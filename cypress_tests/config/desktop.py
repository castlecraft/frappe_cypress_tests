from frappe import _

def get_data():
	return [
		{
			"module_name": "Cypress Tests",
			"type": "module",
			"label": _("Cypress Tests")
		}
	]
