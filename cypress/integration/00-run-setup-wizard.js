context('Run Setup Wizard', () => {
  before(() => {
      cy.login('Administrator', 'admin');
  });

  it('fill setup wizard', () => {
      cy.visit('/app/setup-wizard/0');
      cy.get('input[data-fieldname="language"]').clear();
      cy.get('input[data-fieldname="language"]').type("English").blur();
      cy.get('input[data-fieldname="country"]').clear();
      cy.get('input[data-fieldname="country"]').type("India").blur();
      cy.get('.next-btn').click();
      cy.get('input[data-fieldname="full_name"]').type("Test User").blur();
      cy.get('input[data-fieldname="email"]').type("user@example.com").blur();
      cy.get('input[data-fieldname="password"]').type("123").blur();
      cy.get('.complete-btn').click();
      cy.wait(30000); // Wait for wizard completion
  });
});
