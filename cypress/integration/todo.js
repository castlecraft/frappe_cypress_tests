context('ToDo', () => {
  before(() => {
      cy.login('user@example.com', '123');
      cy.visit('/app');
  });

  it('creates a new todo', () => {
      cy.visit('/app/todo/new-todo-1');
      cy.fill_field('description', 'this is a test todo', 'Text Editor').blur();
      cy.wait(500); // wait for ToDo creation.
      cy.get('.page-title').should('contain', 'Not Saved');
      cy.get('.primary-action').click();
      cy.visit('/app/todo');
      cy.get('.list-row').should('contain', 'this is a test todo');
  });
});
