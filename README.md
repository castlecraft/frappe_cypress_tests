## Cypress Tests

Sample Cypress Tests

## Prerequisites to run tests in frappe_docker/development

Setup `extra_hosts` in `.devcontainer/docker-compose.yml`'s `frappe` container. It should point to site name. e.g. `app.localhost:172.17.0.1`


Install prerequisites

```bash
sudo apt-get update && sudo apt-get install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
```

Install chrome

```bash
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O /tmp/chrome.deb
sudo apt install -y /tmp/chrome.deb && rm /tmp/chrome.deb
```

## Run tests

```bash
bench --site app.localhost run-ui-tests --headless cypress_tests
```

#### License

MIT